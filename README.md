# QRCodeJS

##### dynamically generate QR codes. Uses [jQuery-qrcode](https://larsjung.de/jquery-qrcode/) (MIT).

###### without jQuery :)

## Doc

### Params 

---

> render

##### render method: 

1. 'canvas'
2. 'image' 
3. 'div'


> minVersion, maxVersion

##### version range: 1 .. 40

> error correction level

1. L
2. M
3. Q
4. H

> left: 0, top: 0,

##### offset in pixel if drawn onto existing canvas

> size

##### size in pixel

> fill

##### code color or image element

> background

##### background color or image element, `null` for transparent background

> text

##### content

> radius


##### corner radius relative to module width: 0.0 .. 0.5

> quiet 

##### quiet zone in modules

> mode

##### modes

1. '0'normal
2. '1'label strip
3. '2'label box
4. '3'image strip
5. '4'image box


> mSize, mPosX, mPosY

##### modes instance size & position

> mTop

##### marge top

> headerT

###### header text

> footerT

###### footer text

> label

##### modes of 'label' content

> fontname

##### modes of 'label' content font family

> fontcolor

##### modes of 'label' content font color

> image

##### modes of 'image' src

---

### Example

```

// render method: `'canvas'`, `'image'` or `'div'`
render: 'canvas',

// version range somewhere in 1 .. 40
minVersion: 1,
maxVersion: 40,

// error correction level: `'L'`, `'M'`, `'Q'` or `'H'`
ecLevel: 'L',

// offset in pixel if drawn onto existing canvas
left: 0,
top: 0,

// size in pixel
size: 200,

// code color or image element
fill: '#F00',

// background color or image element, `null` for transparent background
background: '#fff',

// content
text: 'no text',

// corner radius relative to module width: 0.0 .. 0.5
radius: 0,

// quiet zone in modules
quiet: 0,

// modes
// 0: normal
// 1: label strip
// 2: label box
// 3: image strip
// 4: image box
mode: 0,

mSize: 0.1,
mPosX: 0.5,
mPosY: 0.5,
mTop: 0,

headerT: null,
footerT: null,

label: 'no label',
fontname: 'sans',
fontcolor: '#000',

image: null

```

to 233
